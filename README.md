# Dépôt d'exemple (Odran) 
# J'ai pullé et je le renvoie si tu vois ce message
# écrit "ponyo" dans la conversation

Pour faire un `fork`, il faut cliquer en haut à droite du projet:

![](./pics/fork.png)


Puis ensuite, pour ajouter un contributeur au dépôt:

![](./pics/members.png)

Taper l'idep de la personne `(1)` et sélectionner des droits de mainteneur `(2)`

![](./pics/members2.png)
